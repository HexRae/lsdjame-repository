﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    public Rigidbody rb;
    public float gravityforce;

    void FixedUpdate()
    {
        rb.AddForce(Vector3.down * gravityforce, ForceMode.Impulse);
    }
}
