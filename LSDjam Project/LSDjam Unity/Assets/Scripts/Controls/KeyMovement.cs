﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyMovement : MonoBehaviour
{

    public float moveSpeed;
    public Rigidbody rb;

    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        Vector3 forwardVelocity = transform.forward * y * moveSpeed;
        Vector3 lateralVelocity = transform.right * x * moveSpeed;
        Vector3 temp = forwardVelocity + lateralVelocity;

        rb.velocity = new Vector3(temp.x, rb.velocity.y, temp.z) ;

        
    }
}
