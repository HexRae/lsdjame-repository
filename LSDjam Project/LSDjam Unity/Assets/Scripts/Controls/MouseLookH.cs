﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLookH : MonoBehaviour
{
    public float turnSpeed;
    public GameObject pitchObj;


    void FixedUpdate()
    {

        float x = Input.GetAxis("Mouse X");
        Vector3 rotationModX = transform.localEulerAngles + (Vector3.up *x* turnSpeed);
        transform.localEulerAngles = rotationModX;

        float y = Input.GetAxis("Mouse Y");
        Vector3 rotationModY = pitchObj.transform.localEulerAngles + (Vector3.right * -y * turnSpeed);
        pitchObj.transform.localEulerAngles = rotationModY;

    }
}
