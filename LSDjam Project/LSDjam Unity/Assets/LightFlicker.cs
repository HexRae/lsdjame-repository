﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    public float target = 0.5f;
    public Light lightRef;
    public float frameRate;

    void Start()
    {
        StartCoroutine(NewTarget());
    }


    IEnumerator NewTarget()
    {
        target = Random.Range(.250f, 0.75f);
        lightRef.intensity = target;
        yield return new WaitForSeconds(frameRate);
        StartCoroutine(NewTarget());
        yield return null;
    }
}
