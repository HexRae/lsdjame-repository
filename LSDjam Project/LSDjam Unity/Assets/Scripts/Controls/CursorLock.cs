﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorLock : MonoBehaviour
{
    public Actionable interactTarget;
    
    public RaycastHit hit;
    public float raycastRange;

    void Start()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Physics.Raycast(transform.position, transform.forward, out hit, raycastRange);
        if (hit.collider == null)
        {
            interactTarget = null;
        }

        if (hit.collider != null)
        {
            interactTarget = hit.collider.GetComponent<Actionable>();
           // TryGetComponent<Actionable>(out interactTarget);
        }

        if (Input.GetButtonDown("Action"))
        {
            if(interactTarget != null)
            {
                interactTarget.Action();
            }
        }

    }

}
