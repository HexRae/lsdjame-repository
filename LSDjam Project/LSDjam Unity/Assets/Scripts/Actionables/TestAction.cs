﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class TestAction : MonoBehaviour, Actionable
{
   public void Action()
    {
        print(gameObject.name+ " ACTIONED");
    }

}
