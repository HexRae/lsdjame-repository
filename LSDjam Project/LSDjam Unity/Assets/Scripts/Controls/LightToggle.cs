﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightToggle : MonoBehaviour
{
    public bool lightOn;
    public bool LightOn
    {
        get
        {
            return lightOn;
        }
        set
        {
            lightOn = value;
            if(value == true)
            {
                flashlight.enabled = true;
            }
            else
            {
                flashlight.enabled = false;
            }
        }
    }
    public Light flashlight;


    void Update()
    {

        if (Input.GetButtonDown("Flashlight"))
        {
            LightOn = !LightOn;
        }
        
    }
}
